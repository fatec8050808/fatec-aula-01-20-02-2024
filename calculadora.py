# importando biblioteca tkinter
from tkinter import *
from tkinter import ttk

# cores 

cor1 = "#0a0a0a" #Preta
cor2 = "#636261" #Cinza
cor3 = "#f7f8fa" #Branca
cor4 = "#0723f5" #Azul
cor5 = "#f04c1a" #Laranja

# Nossa janela
janela =Tk()
janela.title("Calculadora")
janela.geometry("235x318")
janela.config(bg=cor1)

# Criando frames
frame_tela = Frame(janela, width=235, height=50,bg=cor2)
frame_tela.grid(row=0,column=0)

frame_corpo = Frame(janela, width=235, height=268,bg=cor2)
frame_corpo.grid(row=1,column=0)

#variavel
todos_valores = ''

# Criando funcao
def entrar_valores(event):
    global todos_valores
    todos_valores = todos_valores + str(event)
    ##resultado = eval(todos_valores)
    #passando valor para tela
    valor_texto.set(todos_valores)

def calcular():
    global todos_valores
    if SyntaxError:
        valor_texto.set('Equação invalida')
    resultado = eval(todos_valores)
    valor_texto.set(resultado)

def limpar_tela():
    global todos_valores
    todos_valores = ''
    valor_texto.set('')
    


#Criando labels
valor_texto = StringVar()
app_label = Label(frame_tela, textvariable=valor_texto,width=16,height=2, padx=7,anchor='e',justify=RIGHT,font=("Ivy 17 bold"),relief=FLAT,bg=cor2, fg=cor3)
app_label.place(x=0,y=10)


b_1 = Button(frame_corpo,command=limpar_tela, text="C", width=11,height=2,bg=cor3 , font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_1.place(x=0,y=15)

b_2 = Button(frame_corpo,command= lambda:entrar_valores("%"), text="%", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_2.place(x=117,y=15)

b_3 = Button(frame_corpo,command= lambda:entrar_valores("/"), text="/", width=5,height=2,bg=cor5, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_3.place(x=176,y=15)

b_4 = Button(frame_corpo,command= lambda:entrar_valores("7"), text="7", width=5,height=2,bg=cor3 , font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_4.place(x=0,y=65)

b_5 = Button(frame_corpo,command= lambda:entrar_valores("8"), text="8", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_5.place(x=59,y=65)

b_6 = Button(frame_corpo,command= lambda:entrar_valores("9"), text="9", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_6.place(x=117,y=65)

b_16 = Button(frame_corpo,command= lambda:entrar_valores("*"), text="*", width=5,height=2,bg=cor5, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_16.place(x=176,y=65)

b_7 = Button(frame_corpo,command= lambda:entrar_valores("4"), text="4", width=5,height=2,bg=cor3 , font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_7.place(x=0,y=115)

b_8 = Button(frame_corpo,command= lambda:entrar_valores("5"), text="5", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_8.place(x=59,y=115)

b_9 = Button(frame_corpo,command= lambda:entrar_valores("6"), text="6", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_9.place(x=117,y=115)

b_10 = Button(frame_corpo,command= lambda:entrar_valores("1"), text="1", width=5,height=2,bg=cor3 , font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_10.place(x=0,y=165)

b_11 = Button(frame_corpo,command= lambda:entrar_valores("2"), text="2", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_11.place(x=59,y=165)

b_12 = Button(frame_corpo,command= lambda:entrar_valores("3"), text="3", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_12.place(x=117,y=165)

b_13 = Button(frame_corpo,command= lambda:entrar_valores("0"), text="0", width=11,height=2,bg=cor3 , font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_13.place(x=0,y=215)

b_14 = Button(frame_corpo,command= lambda:entrar_valores(","), text=",", width=5,height=2,bg=cor3, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_14.place(x=117,y=215)

b_15 = Button(frame_corpo,command= calcular, text="=", width=5,height=2,bg=cor5, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_15.place(x=176,y=215)

b_17 = Button(frame_corpo,command= lambda:entrar_valores("-"), text="-", width=5,height=2,bg=cor5, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_17.place(x=176,y=115)

b_18 = Button(frame_corpo,command= lambda:entrar_valores("+"), text="+", width=5,height=2,bg=cor5, font=("Ivy 13 bold"),relief=RAISED,overrelief=RIDGE)
b_18.place(x=176,y=165)

janela.mainloop()
